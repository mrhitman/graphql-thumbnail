## Installation help info

```
	// for local env
	cp .env.example .env
	// change .env to your credentials
	yarn install 
	yarn start:dev

	// or using docker
	docker-compose up -d 
```
   
